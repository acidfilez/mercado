//
//  PaymentViewController.h
//  mercado
//
//  Created by Magno Cardonas on 12/13/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "PaymentMethod.h"

@interface PaymentViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *paymentMethods;

@property (nonatomic, strong) NSNumberFormatter *moneyChileFormatter;

@end
