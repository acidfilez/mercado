//
//  PaymentViewController.m
//  mercado
//
//  Created by Magno Cardonas on 12/13/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "PaymentViewController.h"
#import "BankViewController.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.moneyChileFormatter = NSNumberFormatter.new;
    self.moneyChileFormatter.numberStyle = kCFNumberFormatterDecimalStyle;
    self.moneyChileFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier: @"es_CL"];
    [self doLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void)doLayout {
    

    // Setup View and Table View
    self.title = @"Metodos de pago";
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadPaymentMethods)];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = UIColor.whiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.view addSubview:self.tableView];

    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view).with.offset(@16);
        make.right.equalTo(self.view).with.offset(@-16);
    }];

    
    
    [self loadPaymentMethods];
}

#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentMethod* paymentMethod = [self.paymentMethods objectAtIndex:indexPath.row];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *amount = [f numberFromString:AppStateManager.sharedAppStateManager.amount];
    
    //Prevent errors on the futures
    if (paymentMethod.maxAllowedAmount < amount) {
        FCAlertView *alert = [[FCAlertView alloc] init];
        [alert makeAlertTypeWarning];
        
        [alert showAlertWithTitle:@"Insuficiente Fondo en este Metodo de Pago"
                     withSubtitle:@"Debe cambiar el monto o usar otro forma de pago"
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        return;
    }
    
    [self pushController:paymentMethod];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.paymentMethods count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"PaymentMethodCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.backgroundColor = [UIColor clearColor];

        UIImageView *imgview = [[UIImageView alloc]initWithFrame: CGRectZero];
        [imgview setContentMode:UIViewContentModeScaleAspectFit];

        //Tip 1: Never ever do this, but this is a mvp so its as fast as i can.
        imgview.tag = 100;
        [cell addSubview:imgview];

        [imgview makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.mas_centerY);
            make.right.equalTo(cell.mas_right).offset(@-16);
        }];
    }

    PaymentMethod* paymentMethod = [self.paymentMethods objectAtIndex:indexPath.row];
    cell.textLabel.text = [paymentMethod name];
    NSString *chileAmount = [self.moneyChileFormatter stringFromNumber: [paymentMethod maxAllowedAmount]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Monto Disponible: $%@", chileAmount];

    //Tip 1: Continuation, now its time to set the image.
    UIImageView *imgview = [cell viewWithTag: 100];
    [imgview sd_setImageWithURL:[NSURL URLWithString: [paymentMethod thumbnail]]
               placeholderImage:nil];

    return cell;
}


#pragma mark - Network

- (void)loadPaymentMethods {
    self.paymentMethods = NSArray.new;
    if(self.isViewLoaded) {
        [self.tableView reloadData];
    }
    
    // Load the object model via RestKit
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    NSDictionary *params = @{@"public_key" :  AppStateManager.sharedAppStateManager.publicKey};

    [objectManager getObjectsAtPath:@"/v1/payment_methods"
                         parameters:params
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                NSArray* paymentMethods = [mappingResult array];
                                NSLog(@"Loaded paymentMethods: %@", paymentMethods);
                                self.paymentMethods = paymentMethods;

                                if(self.isViewLoaded) {
                                    [self.tableView reloadData];
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                NSLog(@"Hit error: %@", error);
                            }];
}

#pragma mark Navigation methods
- (void)pushController: (PaymentMethod *)paymentMethod {
    NSLog(@"%@", paymentMethod);
    AppStateManager.sharedAppStateManager.paymentMethod = paymentMethod;

    BankViewController *bankViewController  = BankViewController.new;    
    [self.navigationController pushViewController:bankViewController animated:YES];
}

#pragma mark EmpyDataSet methods
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Cargando metodos de pago";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id quam aliquam.";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityView startAnimating];
    return activityView;
}

@end
