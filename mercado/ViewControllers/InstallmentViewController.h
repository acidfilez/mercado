//
//  InstallmentViewController.h
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentViewController.h"
#import "Bank.h"
#import "Installment.h"

@interface InstallmentViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *installments;

@end
