//
//  WalkViewController.m
//  mercado
//
//  Created by Magno Cardonas on 12/15/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "WalkViewController.h"
#import "NDIntroView.h"
#import "AmountViewController.h"

@interface WalkViewController () <NDIntroViewDelegate>

@property (strong, nonatomic) NDIntroView *introView;

@property (weak, nonatomic) IBOutlet UIButton *restartButton;


@end

@implementation WalkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Onboarding";
    
    // Do any additional setup after loading the view.
    [self.restartButton setBackgroundColor:[UIColor colorWithRed:70.f/255.f green:130.f/255.f blue:180.f/255.f alpha:1.f]];
    self.restartButton.layer.cornerRadius = 5.f;
    
    [self startIntro];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)restartIntroViewButtonPressed:(id)sender {
    [self startIntro];
}


#pragma mark - NDIntroView methods

-(void)startIntro {
    NSArray *pageContentArray = @[@{kNDIntroPageTitle : @"Ingrasar Monto",
                                    kNDIntroPageDescription : @"Primero ingresa el monto deseado a pagar.",
                                    kNDIntroPageImageName : @"parallax"
                                    },
                                  @{kNDIntroPageTitle : @"Metodo de pago",
                                    kNDIntroPageDescription : @"Encuntra el metodo de pago que mas te convenga",
                                    kNDIntroPageImageName : @"workitout"
                                    },
                                  @{kNDIntroPageTitle : @"Banco",
                                    kNDIntroPageDescription : @"Eligue el banco con el cual haras cargo a tu tarjeta",
                                    kNDIntroPageImageName : @"colorskill"
                                    },
                                  @{kNDIntroPageTitle : @"Cuotas",
                                    kNDIntroPageDescription : @"Finalmente debes elegir la cantidad de cuotas para tu pago",
                                    kNDIntroPageImageName : @"appreciate"
                                    }
                                  ];
    self.introView = [[NDIntroView alloc] initWithFrame:self.view.frame parallaxImage:[UIImage imageNamed:@"parallaxBgImage"] andData:pageContentArray];
    self.introView.delegate = self;
    [self.view addSubview:self.introView];
}

-(void)launchAppButtonPressed {
    
    AmountViewController *viewController = [[AmountViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:NO];
    
}

@end
