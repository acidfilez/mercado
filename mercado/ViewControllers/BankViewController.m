//
//  BankViewController.m
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "BankViewController.h"
#import "InstallmentViewController.h"

@interface BankViewController ()

@end

@implementation BankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self doLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void)doLayout {
    // Setup View and Table View
    self.title = @"Bancos";
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadBanks)];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = UIColor.whiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.view addSubview:self.tableView];

    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view).with.offset(@16);
        make.right.equalTo(self.view).with.offset(@-16);
    }];

    
    
    [self loadBanks];
}

#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Bank* bank = [self.banks objectAtIndex:indexPath.row];
    NSLog(@"%@", bank);
    [self pushController:bank];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.banks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"BankCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.backgroundColor = [UIColor clearColor];

        UIImageView *imgview = [[UIImageView alloc]initWithFrame: CGRectZero];
        [imgview setContentMode:UIViewContentModeScaleAspectFit];

        //Tip 1: Never ever do this, but this is a mvp so its as fast as i can.
        imgview.tag = 100;
        [cell addSubview:imgview];

        [imgview makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.mas_centerY);
            make.right.equalTo(cell.mas_right).offset(@-16);
        }];
    }

    Bank* bank = [self.banks objectAtIndex:indexPath.row];
    cell.textLabel.text = [bank name];

    //Tip 1: Continuation, now its time to set the image.
    UIImageView *imgview = [cell viewWithTag: 100];
    [imgview sd_setImageWithURL:[NSURL URLWithString: [bank thumbnail]]
               placeholderImage:nil];

    return cell;
}


#pragma mark - Network

- (void)loadBanks {
    
    self.banks = NSArray.new;
    if(self.isViewLoaded) {
        [self.tableView reloadData];
    }
    
    // Load the object model via RestKit
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    NSDictionary *params = @{@"public_key" :  AppStateManager.sharedAppStateManager.publicKey,
                             @"payment_method_id" : AppStateManager.sharedAppStateManager.paymentMethod.id,
                             };


    [objectManager getObjectsAtPath:@"/v1/payment_methods/card_issuers"
                         parameters:params
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                NSArray* banks = [mappingResult array];
                                NSLog(@"Loaded banks: %@", banks);
                                self.banks = banks;

                                if(self.isViewLoaded) {
                                    [self.tableView reloadData];
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                NSLog(@"Hit error: %@", error);
                            }];
}

#pragma mark Navigation methods
- (void)pushController: (Bank *)bank {
    AppStateManager.sharedAppStateManager.bank = bank;
    InstallmentViewController *installmentViewController  = InstallmentViewController.new;

    [self.navigationController pushViewController:installmentViewController animated:YES];
}

#pragma mark EmpyDataSet methods
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Cargando bancos";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id quam aliquam.";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityView startAnimating];
    return activityView;
}

@end
