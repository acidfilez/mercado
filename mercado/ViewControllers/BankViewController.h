//
//  BankViewController.h
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "PaymentMethod.h"
#import "Bank.h"

@interface BankViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate> 

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *banks;


@end
