//
//  AmountViewController.m
//  mercado
//
//  Created by Magno Cardonas on 12/12/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "AmountViewController.h"
#import "PaymentMethod.h"

@interface AmountViewController ()
@property(nonatomic) MDCTextInputControllerDefault *montoController;
@property(nonatomic) MDCTextField *txtAmount;
@end

@implementation AmountViewController

#pragma mark - LifeCycle

- (id)init {
    self = [super init];
    if (!self) return nil;
    
    self.title = @"Monto";
    
    AppStateManager.sharedAppStateManager.ended = @0;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self doLayout];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Handle Keyboard when tap outside
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)viewDidAppear:(BOOL)animated
{
    //Check if the flow ended
    if (AppStateManager.sharedAppStateManager.ended.intValue == 1) {
        
        [self endedBudget];
        self.txtAmount.text = @"";
        AppStateManager.sharedAppStateManager.ended = @0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getter Methods

- (void)doLayout {
    
    self.txtAmount = MDCTextField.new;
    self.txtAmount.placeholder = @"Ingrese el monto";
    self.txtAmount.clearButtonMode = UITextFieldViewModeUnlessEditing;
    self.txtAmount.backgroundColor = [UIColor whiteColor];
    self.txtAmount.keyboardType = UIKeyboardTypeNumberPad;
    
    self.montoController = [[MDCTextInputControllerDefault alloc] initWithTextInput: self.txtAmount];

    [self.view addSubview: self.txtAmount];
    
    MDCFlatButton *flatButton = MDCFlatButton.new;
    [flatButton setTitle:@"Continuar" forState:UIControlStateNormal];
    [flatButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    [flatButton sizeToFit];
    [flatButton addTarget:self
                   action:@selector(tapNext)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:flatButton];
    
    
    [flatButton makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
        make.left.equalTo(self.view).with.offset(@16);
        make.right.equalTo(self.view).with.offset(@-16);
        make.height.equalTo(@60);
    }];
    
    [self.txtAmount makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(flatButton.mas_top);
        make.left.equalTo(self.view).with.offset(@16);
        make.right.equalTo(self.view).with.offset(@-16);
        make.height.equalTo(@100);
    }];
    
}

#pragma mark - Utils Methods

//Just end editing if tap detected outside the text
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
}

//Just end editing if tap detected outside the text
- (void) tapNext {
    NSLog(@"%@", self.txtAmount.text);

    AppStateManager.sharedAppStateManager.amount = self.txtAmount.text;

    PaymentViewController *vc  = PaymentViewController.new;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void) endedBudget {
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeSuccess];
    alert.cornerRadius = 4;
    
    alert.titleFont = [UIFont fontWithName:@"SFUIText" size:10.0];
    alert.subtitleFont = [UIFont fontWithName:@"SFUIText" size:10.0];
    
    NSString *title = [NSString stringWithFormat:@"Se realizo pago con metodo '%@', atravez '%@'",
                       AppStateManager.sharedAppStateManager.paymentMethod.name, AppStateManager.sharedAppStateManager.bank.name];
    
    NSString *subtitle = [NSString stringWithFormat:@"En: %@",
                          AppStateManager.sharedAppStateManager.installment.recommendedMessage];
    
    [alert showAlertInWindow:self.view.window
                   withTitle:title
                withSubtitle: subtitle
             withCustomImage:nil
         withDoneButtonTitle:nil
                  andButtons:nil];
}

@end

