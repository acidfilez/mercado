//
//  InstallmentViewController.m
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "InstallmentViewController.h"


@interface InstallmentViewController ()

@end

@implementation InstallmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self doLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void)doLayout {
    // Setup View and Table View
    self.title = @"Cuotas";
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadInstallments)];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = UIColor.whiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.view addSubview:self.tableView];

    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view).with.offset(@16);
        make.right.equalTo(self.view).with.offset(@-16);
    }];

    
    
    [self loadInstallments];
}

#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Installment* installment = [self.installments objectAtIndex:indexPath.row];
    [self pushController:installment];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.installments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"InstallmentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.backgroundColor = [UIColor clearColor];

        UIImageView *imgview = [[UIImageView alloc]initWithFrame: CGRectZero];
        [imgview setContentMode:UIViewContentModeScaleAspectFit];

        //Tip 1: Never ever do this, but this is a mvp so its as fast as i can.
        imgview.tag = 100;
        [cell addSubview:imgview];

        [imgview makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.mas_centerY);
            make.right.equalTo(cell.mas_right).offset(@-16);
        }];
    }

    Installment* installment = [self.installments objectAtIndex:indexPath.row];
    cell.textLabel.text = [installment recommendedMessage];

    return cell;
}


#pragma mark - Network

- (void)loadInstallments {
    self.installments = NSArray.new;
    if(self.isViewLoaded) {
        [self.tableView reloadData];
    }
    
    // Load the object model via RestKit
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    NSDictionary *params = @{@"public_key" : AppStateManager.sharedAppStateManager.publicKey,
                             @"amount" : AppStateManager.sharedAppStateManager.amount,
                             @"payment_method_id" : AppStateManager.sharedAppStateManager.paymentMethod.id,
                             @"issuer.id" : AppStateManager.sharedAppStateManager.bank.id,
                             };

    [objectManager getObjectsAtPath:@"/v1/payment_methods/installments"
                         parameters:params
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {

                                //Okey mapping to complex, lets go old days.

                                //Get response body as string
                                NSString *jsonString = operation.HTTPRequestOperation.responseString;

                                if (jsonString == nil || [jsonString isEqual: @""] || [jsonString isEqual: @"[]"]) {
                                    FCAlertView *alert = [[FCAlertView alloc] init];
                                    [alert makeAlertTypeCaution];
                                    
                                    [alert showAlertWithTitle:@"Algo Malo Paso"
                                                 withSubtitle:@"Talvez un error en el monto ?"
                                              withCustomImage:nil
                                          withDoneButtonTitle:nil
                                                   andButtons:nil];
                                    return;
                                }

                                //Now serialize it
                                NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                                //Go to payer_costs, and get all the items
                                NSArray *payerCosts = json[0][@"payer_costs"];

                                //Clean and populate the installments, and fill it with recommendedMessages.
                                self.installments = NSMutableArray.new;
                                for (id jsonPayerCost in payerCosts)
                                {
                                    Installment *installment = Installment.new;
                                    installment.recommendedMessage = jsonPayerCost[@"recommended_message"];
                                    [self.installments addObject:installment];
                                }

                                //Reload cuotas again
                                if(self.isViewLoaded) {
                                    [self.tableView reloadData];
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                NSLog(@"Hit error: %@", error);
                            }];
}


#pragma mark Navigation methods
- (void)pushController: (Installment *)installment {
    AppStateManager.sharedAppStateManager.ended = @1;
    AppStateManager.sharedAppStateManager.installment = installment;
    
    UIViewController *second = self.navigationController.childViewControllers[1];
    [self.navigationController popToViewController:second animated:YES];
}

#pragma mark EmpyDataSet methods
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Cargando cuotas";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id quam aliquam.";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityView startAnimating];
    return activityView;
}

@end
