//
//  PaymentMethod.h
//  mercado
//
//  Created by Magno Cardonas on 12/12/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//


@interface PaymentMethod : NSObject

/**
 *
 */
@property (nonatomic, copy) NSString *id;

/**
 *
 */
@property (nonatomic, copy) NSString *name;

/**
 *
 */
@property (nonatomic, copy) NSString *paymentTypeId;


/**
 *
 */
@property (nonatomic, copy) NSString *status;


/**
 *
 */
@property (nonatomic, copy) NSString *secureThumbnail;

/**
 *
 */
@property (nonatomic, copy) NSString *thumbnail;

/**
 *
 */

@property (nonatomic, copy) NSNumber *minAllowedAmount;

/**
 *
 */
@property (nonatomic, copy) NSNumber *maxAllowedAmount;

@end
