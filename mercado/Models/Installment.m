//
//  Installment.m
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "Installment.h"

@implementation Installment

- (NSString *)description
{
    return [NSString stringWithFormat:@"Installment (ID: %@)", self.recommendedMessage];
}

@end
