//
//  Bank.m
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "Bank.h"

@implementation Bank

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ (ID: %@)", self.name, self.id];
}

@end
