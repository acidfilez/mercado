//
//  Bank.h
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

@interface Bank : NSObject

/**
 *
 */
@property (nonatomic, copy) NSString *id;

/**
 *
 */
@property (nonatomic, copy) NSString *name;


/**
 *
 */
@property (nonatomic, copy) NSString *secureThumbnail;

/**
 *
 */
@property (nonatomic, copy) NSString *thumbnail;

@end
