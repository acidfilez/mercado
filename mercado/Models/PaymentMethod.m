//
//  PaymentMethod.m
//  mercado
//
//  Created by Magno Cardonas on 12/12/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "PaymentMethod.h"

@implementation PaymentMethod

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ (ID: %@)", self.name, self.id];
}

@end
