//
//  AppStateManager.m
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "AppStateManager.h"

@implementation AppStateManager

@synthesize amount;
@synthesize paymentMethod;
@synthesize bank;
@synthesize installment;
@synthesize ended;
@synthesize publicKey;

#pragma mark -
#pragma mark Class Methods
+ (AppStateManager *)sharedAppStateManager {
    static AppStateManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });

    return _sharedInstance;
}

@end
