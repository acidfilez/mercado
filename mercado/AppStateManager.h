//
//  AppStateManager.h
//  mercado
//
//  Created by Magno Cardonas on 12/14/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "Bank.h"
#import "Installment.h"

@interface AppStateManager : NSObject {
    NSString *amount;
    PaymentMethod *paymentMethod;
    Bank *bank;
    Installment *installment;
    
    NSString *publicKey;
    
    NSNumber *ended;
}

@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) PaymentMethod *paymentMethod;
@property (nonatomic, retain) Bank *bank;
@property (nonatomic, retain) Installment *installment;

@property (nonatomic, retain) NSString *publicKey;

@property (nonatomic, retain) NSNumber *ended;

#pragma mark -
#pragma mark Class Methods
+ (AppStateManager *)sharedAppStateManager;

@end
