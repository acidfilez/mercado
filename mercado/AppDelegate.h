//
//  AppDelegate.h
//  mercado
//
//  Created by Magno Cardonas on 12/12/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "AmountViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

