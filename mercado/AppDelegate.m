//
//  AppDelegate.m
//  mercado
//
//  Created by Magno Cardonas on 12/12/17.
//  Copyright © 2017 Homer Simpson. All rights reserved.
//

#import "AppDelegate.h"
#import "PaymentMethod.h"
#import "Bank.h"
#import "Installment.h"

#import "WalkViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - App LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initWindow];

    [self initRestkit];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Helpers

- (void)initWindow {

    self.window = [[UIWindow alloc] initWithFrame: UIScreen.mainScreen.bounds];
    self.window.backgroundColor = UIColor.whiteColor;

    //Create navigation, and initial controller
    AmountViewController *viewController = [[AmountViewController alloc] init];
    
    WalkViewController *viewController2 = [[WalkViewController alloc] init];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController2];

    //display initial controller
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
}

- (void)initRestkit {
    
    AppStateManager.sharedAppStateManager.publicKey = @"444a9ef5‐8a6b‐429f‐abdf‐587639155d88";

    RKLogConfigureByName("RestKit/Network*", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);

    //let AFNetworking manage the activity indicator
    [AFRKNetworkActivityIndicatorManager sharedManager].enabled = YES;

    // Initialize HTTPClient
    NSURL *baseURL = [NSURL URLWithString:@"https://api.mercadopago.com"];
    AFRKHTTPClient* client = [[AFRKHTTPClient alloc] initWithBaseURL:baseURL];

    //we want to work with JSON-Data
    [client setDefaultHeader:@"Accept" value:RKMIMETypeJSON];

    // Initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];

    // Setup our object mappings

    //Mapping for payment
    RKObjectMapping *paymentMethodMapping = [RKObjectMapping mappingForClass:[PaymentMethod class]];
    [paymentMethodMapping addAttributeMappingsFromDictionary:@{
                                                        @"id" : @"id",
                                                        @"name" : @"name",
                                                        @"payment_type_id" : @"paymentTypeId",
                                                        @"status" : @"status",
                                                        @"secure_thumbnail" : @"secureThumbnail",
                                                        @"thumbnail" : @"thumbnail",
                                                        @"min_allowed_amount" : @"minAllowedAmount",
                                                        @"max_allowed_amount" : @"maxAllowedAmount",
                                                        }];


    // Register our mappings with the provider using a response descriptor
    RKResponseDescriptor *paymentMethodResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:paymentMethodMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@"/v1/payment_methods"
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:paymentMethodResponseDescriptor];


    //Mapping for bank
    RKObjectMapping *bankMapping = [RKObjectMapping mappingForClass:[Bank class]];
    [bankMapping addAttributeMappingsFromDictionary:@{
                                                               @"id" : @"id",
                                                               @"name" : @"name",
                                                               @"secure_thumbnail" : @"secureThumbnail",
                                                               @"thumbnail" : @"thumbnail",
                                                               }];


    // Register our mappings with the provider using a response descriptor
    RKResponseDescriptor *bankResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bankMapping
                                                                                                         method:RKRequestMethodGET
                                                                                                    pathPattern:@"/v1/payment_methods/card_issuers"
                                                                                                        keyPath:nil
                                                                                                    statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:bankResponseDescriptor];



    //Mapping for installment, this failed sadly
    RKObjectMapping *installmentMapping = [RKObjectMapping mappingForClass:[Installment class]];
    [installmentMapping addAttributeMappingsFromDictionary:@{

                                                      }];


    // Register our mappings with the provider using a response descriptor
    RKResponseDescriptor *installResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:installmentMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:@"/v1/payment_methods/installments"
                                                                                               keyPath:nil
                                                                                           statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:installResponseDescriptor];

}

@end
